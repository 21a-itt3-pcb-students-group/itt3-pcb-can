**WW37**

- **Summary of activities**
- Read and Complete all exercises
- Project log updated
- Design rule check performed
- Netlist created
- Board outline created
- Mounting holes placed on board
- Catch up on exercises from previous weeks



- **Lessons learned**
- Design rule check - **DONE**
- Netlist - **DONE**
- Board outline - **DONE**
- Create mounting holes - **DONE**
- Finished the crystal oscilator subsystem - **DONE**

- **Ressources used**
- https://resources.orcad.com/orcad-capture-tutorials/orcad-capture-tutorial-09-perform-a-schematic-drc
- https://www.youtube.com/playlist?list=PLDclr_SCaTAwUiJYgKDOUQvv6TJRj6fXx
- https://resources.orcad.com/orcad-pcb-editor-tutorials/orcad-pcb-editor-tutorial-02-generate-a-pcb-editor-netlist-in-orcad-capture
- https://resources.orcad.com/orcad-pcb-editor-tutorials/orcad-pcb-editor-tutorial-03-set-up-pcb-design-and-draw-a-board-outline
- https://resources.orcad.com/orcad-pcb-editor-tutorials/orcad-pcb-editor-tutorial-04-create-and-place-mechanical-symbols
