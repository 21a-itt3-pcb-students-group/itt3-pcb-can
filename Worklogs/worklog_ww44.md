**WW44**

- **Summary of activities**
- Board and layers setup completed

- **Lessons learned**
- Board outline - **DONE**
- Mounting holes using mechanical symbols - **DONE**
- Component placement - **DONE**
- Setup constraints in Orcad PCB editor - **DONE**
- Copper plane and shapes - **DONE**
- Finishing the Temperture Sensor Subsystem - **DONE**
- Adding smd footprints to all of the components - **DONE**
- Adding Header Pins - **DONE**


- **Ressources used**
- https://resources.orcad.com/orcad-pcb-editor-tutorials/orcad-pcb-editor-tutorial-03-set-up-pcb-design-and-draw-a-board-outline
- https://resources.orcad.com/orcad-pcb-editor-tutorials/orcad-pcb-editor-tutorial-04-create-and-place-mechanical-symbols
- https://resources.orcad.com/orcad-pcb-editor-tutorials/orcad-pcb-editor-tutorial-05-place-components
- https://www.altium.com/design-manufacturing-resources
- https://www.youtube.com/playlist?list=PLDclr_SCaTAwUiJYgKDOUQvv6TJRj6fXx
- https://resources.pcb.cadence.com/blog/design-for-manufacturing-or-dfm-analysis-pcb-dfm-process-slp
- https://jlcpcb.com/capabilities/Capabilities
- https://resources.orcad.com/orcad-pcb-editor-tutorials/orcad-pcb-editor-tutorial-06-set-up-differential-pairs-and-constraint-manager
- https://resources.orcad.com/orcad-pcb-editor-tutorials/orcad-pcb-editor-tutorial-07-create-copper-plane-and-shapes
