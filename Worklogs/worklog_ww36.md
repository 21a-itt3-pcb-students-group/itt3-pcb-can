**WW36**

- **Summary of activities**
- Making the part on orcad. 
- Searching the parts and the prices.
- Learning how to find different footprints on websites.



- **Lessons learned**
- Component research - **DONE**
- Make RP2040 chip - **DONE**
- Make power supply in orcad - **DONE**
- Make flash memory in orcad - **DONE**

- **Ressources used**
1. https://www.electronics-notes.com/articles/electronic_components/surface-mount-technology-smd-smt/packages.php
2. https://www.pcbonline.com/blog/pcb-smt-components.html
3. https://en.wikipedia.org/wiki/List_of_integrated_circuit_packaging_types#PIN-PITCH
4. https://en.wikipedia.org/wiki/Surface-mount_technology
5. https://www.youtube.com/watch?v=zFTEcsgR8iE&list=PLDclr_SCaTAwUiJYgKDOUQvv6TJRj6fXx&index=5
6. https://resources.orcad.com/orcad-capture-tutorials
7.  https://www.youtube.com/watch?v=CYtn_WWPizo&list=PLDclr_SCaTAwUiJYgKDOUQvv6TJRj6fXx&index=7
8. https://www.youtube.com/watch?v=s8mYUTix3ao
9. https://www.youtube.com/playlist?list=PLDclr_SCaTAwUiJYgKDOUQvv6TJRj6fXx
10. https://www.ultralibrarian.com/
11. https://componentsearchengine.com/
12. https://octopart.com/
13. https://www.snapeda.com/home/
