**WW35**

- **Summary of activities**
- Nordcad beginners course completed
- Schematic completed
- Subcircuits simulated and output documented



- **Lessons learned**
- Inside a PCB soldering factory video watched - **DONE**
- Nordcad beginners course videos - **DONE**
- Draw schematic - **DONE**
- Simulation - **DONE**

- **Ressources used**
1. https://youtu.be/24ehoo6RX8w
2. https://www.nordcad.eu/student-forum/beginners-course/
