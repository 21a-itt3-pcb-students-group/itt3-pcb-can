**WW45**

- **Summary of activities**
- PCB routed
- Errors cleared using DRC
- Project log updated on gitlab

- **Lessons learned**
- Route the board - **DONE**
- Silkscreen - **DONE**
- Clear DRC errors - **DONE**
- Design sanity check - **DONE**
- Create Gerber and Drill files - **DONE**
- Inspect gerbers and drill file - **DONE**
- Finish the board outline
- Finish the copper layer
- Placed all of the components
- Made all of the traces
- Reedit the power subsystem(one of the components cost a lot at jlcpcb and everywhere else it wasnt in stock)
- Placed the order


- **Ressources used**
- https://www.edn.com/ten-best-practices-of-pcb-design/
- https://www.pannam.com/blog/pcb-design-guide/
- https://www.4pcb.com/trace-width-calculator.html
- https://www.youtube.com/playlist?list=PLDclr_SCaTAwUiJYgKDOUQvv6TJRj6fXx
- https://www.ucamco.com/en/gerber
- https://en.wikipedia.org/wiki/PCB_NC_formats
- https://sourceforge.net/projects/gerbv/
- https://www.gerbview.com/
- https://jlcpcb.com/
