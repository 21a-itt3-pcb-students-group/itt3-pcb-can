**WW34**

- **Summary of activities**
- Searching the circuits and deciding which to build. 
- Checking if our orcad license is working. 

- **Lessons learned**
- Circuit research and decide on the circuit - **DONE**
- Create a block diagram with logical subcircuits and specifications for input and output values - **DONE**
- Setup gitlab documentation project - **DONE**
- Project log - **DONE**
- OrCad installation and license - **DONE**
- I watch the videos provided by Nikolaj - **DONE**

- **Ressources used**

1. https://eal-itt.gitlab.io/21a-itt3-pcb/other-docs/circuit_catalogue.html
2. https://www.digikey.com/reference-designs/en
3. https://datasheets.raspberrypi.org/rp2040/hardware-design-with-rp2040.pdf
4. https://datasheets.raspberrypi.org/rp2040/rp2040-datasheet.pdf
5. https://youtu.be/X00Cm5LMNQk
