# ITT3-PCB-Can Gabriel Bekil

**1. Chosen Circuit**
- RP2040 IoT development board


- [x] **Week 34**
 - [x] Exercise 1 - Circuit research
 - [x] Exercise 2 - Setup gitlab documentation project
 - [x] Exercise 3 - Project log
 - [x] Exercise 4 - OrCad installation and license


- [x] **Week 35**
 - [x] Exercise 1 - Nordcad beginners course
 - [x] Exercise 2 - Draw schematic
 - [x] Exercise 3 - Simulation



- [x] **Week 36**
 - [x] Exercise 1 - SMT component packages
 - [x] Exercise 2 - Component research
 - [x] Exercise 3 - Footprints



- [x] **Week 37**
 - [x] Exercise 1 - Design rule check
 - [x] Exercise 2 - Netlist
 - [x] Exercise 3 - Board outline
 - [x] Exercise 4 - Create mounting holes



- [x] **Week 38**
 - [x] Exercise 3 - Create mounting holes
 - [x] Exercise 2 - Nordcad workshop



- [x] **Week 39**
 - [x] Exercise 1 - Course exercises overview
 - [x] Exercise 2 - Design for manufacturing workshop


- [x] **Week 44**
 - [x] Exercise 1 - Board outline
 - [x] Exercise 2 - Mounting holes using mechanical symbols
 - [x] Exercise 3 - Component placement
 - [x] Exercise 4.1 - Design Guidelines for Successful Manufacturing
 - [x] Exercise 4.2 - Setup constraints in Orcad PCB editor
 - [x] Exercise 5 - Copper plane and shapes


- [x] **Week 45**
 - [x] Exercise 1 - Route the board
 - [x] Exercise 2 - Silkscreen
 - [x] Exercise 3 - Clear DRC errors
 - [x] Exercise 4 - Design sanity check
 - [x] Exercise 5 - Create Gerber and Drill files
 - [x] Exercise 6 - Inspect gerbers and drill file
 - [x] Exercise 7 - Order from JLC PCB


- [x] **Week 46**
 - [x] Exercise 1 - Course exercises overview
 - [x] Exercise 2 - Documentation



- [x] **Week 47**
 - [x] Exercise 1 - Assemble produced PCB



- [x] **Week 48**
 - [ ] Exercise 1 - PCB revision 2 in KiCad



- [x] **Week 49**
 - [x] Exercise 1 - Report
